Aufgabe: The List API

1. Beim ersten Request ist die Syntax zwar gültig, entspricht aber nicht dem Schema. Beim zweiten Request wurde die Abfrage nicht an den Server gesendet weil die Syntax ungültig ist. (es fehlt ein Subfield-Name)

2. Hier wird der Request an den Server gesendet das das Subfield vorhanden ist. Nach dessen Validierung sendet der Server die Antwort mit dem Fehler. (400 Bad Request)

3. Sie können ähnlich aussehen und unterschiedliche Inhalte haben. Die beiden Felder werden als Properties des Data-Objects zurückgegeben.